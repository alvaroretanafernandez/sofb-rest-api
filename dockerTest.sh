

#!/bin/bash

BLUE='\033[0;36m'
GREY='\033[0;37m'

echo "${BLUE}[${GREY}test${BLUE}] --------------------------------------------------------"
echo "${BLUE}[${GREY}test${BLUE}] ==================> Starting server tests ..."
echo "${BLUE}[${GREY}test${BLUE}] --------------------------------------------------------"


echo "${BLUE}[${GREY}test${BLUE}] Docker : ==================> Remove old images ..."
docker rmi $(docker images -f dangling=true -q)
echo "${BLUE}[${GREY}test${BLUE}] Docker : ==================> Build image ..."
docker-compose -f docker-compose.test.yml build
echo "${BLUE}[${GREY}test${BLUE}] Docker : ==================> Run image ..."
docker-compose -f docker-compose.test.yml up -d
echo "${BLUE}[${GREY}test${BLUE}] Npm : ==================> Set NODE_ENV=test ..."
export NODE_ENV=test
echo "${BLUE}[${GREY}test${BLUE}] Npm : ==================> Run npm test:server suite ..."
 npm run test:server || exit 1

echo "${BLUE}[${GREY}test${BLUE}] Docker : ==================> Stop docker image ..."
docker-compose -f docker-compose.test.yml stop

echo "${BLUE}[${GREY}test${BLUE}] --------------------------------------------------------"
echo "${BLUE}[${GREY}test${BLUE}] ==================> Test script finished!! "
echo "${BLUE}[${GREY}test${BLUE}] --------------------------------------------------------"
