'use strict';

var validator = require('validator'),
    path = require('path'),
    config = require(path.resolve('./config/config'));

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
    var safeUserObject = null;
    var welcome = {
        site: 'http://api.stokedonfixedbikes.com/',
        version: '4.0',
        description: 'The public REST API of stokedonfixedbikes websites.',
        resources: {
            active: {
                magazines: '/magazines',
                magazine: '/magazines/:id',
                videos: '/videos',
                video: '/videos/:id',
                links: '/links',
                link: '/links/:id'
            }
        },
        docs: 'https://apidocs.stokedonfixedbikes.com/',
        web: 'https://stokedonfixedbikes.com',
        webv3: 'https://v3.stokedonfixedbikes.com',
        webv4: 'https://v4.stokedonfixedbikes.com',
        mobile: 'https://ionic.stokedonfixedbikes.com',
        author: 'aretanafernandez@gmail.com',
        lastUpdate: '2017-02-21T21:31:12.549Z'

    };

    res.json(welcome);
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
    res.status(500).json({
        error: 'Oops! Something went wrong...'
    });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {
    var forbidden = {
        site: 'https://api.stokedonfixedbikes.com/',
        error: {
            url: req.url,
            message: 'This are not the droids you are looking for.',
            code: 404
        }
    };
    res.status(404).json(forbidden);
};
