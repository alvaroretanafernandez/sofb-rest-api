'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  Magazine = mongoose.model('Magazine');

/**
 * Globals
 */
var user,
  magazine;

/**
 * Unit tests
 */
describe('Magazine Model Unit Tests:', function() {
    beforeEach(function(done) {

        magazine = new Magazine({
            url: 'Mag Name'
        });

        done();
    });
  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
       magazine.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) {
      magazine.url = '';

       magazine.save(function(err) {
        should.not.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Magazine.remove().exec(function() {
        done();
    });
  });
});
