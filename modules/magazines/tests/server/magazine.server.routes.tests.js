'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Magazine = mongoose.model('Magazine'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  magazine;

/**
 * Magazine routes tests
 */
describe('Magazine CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Magazine
      magazine = {
        id: 1,
        url: 'Magazine name'
      };

      done();
  });


  it('should be able to get a list of Magazines if not signed in', function (done) {
    // Create new Magazine model instance
    var magazineObj = new Magazine(magazine);

    // Save the magazine
    magazineObj.save(function () {
      // Request Magazines
      request(app).get('/api/magazines')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Magazine if not signed in', function (done) {
    // Create new Magazine model instance
    var magazineObj = new Magazine(
        {
            id: 1,
            url: 'Magazine name'
        }
    );

    // Save the Magazine
    magazineObj.save(function (err, mag) {

        console.log(mag.id);

      request(app).get('/api/magazines/' + magazineObj.id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('url', magazine.url);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Magazine with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/magazines/' + 35)
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Magazine with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Magazine which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Magazine
    request(app).get('/api/magazines/' + 99)
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Magazine with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should not be able to delete an Magazine if not admin signed in', function (done) {
    // Set Magazine user
    magazine.user = user;

    // Create new Magazine model instance
    var magazineObj = new Magazine(magazine);

    // Save the Magazine
    magazineObj.save(function () {
      // Try deleting Magazine
      request(app).delete('/api/magazines/' + magazineObj.id)
        .expect(403)
        .end(function (magazineDeleteErr, magazineDeleteRes) {
          // Set message assertion
          (magazineDeleteRes.body.message).should.match('User is not authorized');

          // Handle Magazine error error
          done(magazineDeleteErr);
        });

    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Magazine.remove().exec(done);
    });
  });
});
