'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Magazine = mongoose.model('Magazine'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Magazine
 */
exports.create = function(req, res) {
  var magazine = new Magazine(req.body);
  magazine.user = req.user;

  magazine.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(magazine);
    }
  });
};

/**
 * Show the current Magazine
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var magazine = req.magazine ? req.magazine.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  magazine.isCurrentUserOwner = req.user && magazine.user && magazine.user._id.toString() === req.user._id.toString();

  res.jsonp(magazine);
};

/**
 * Update a Magazine
 */
exports.update = function(req, res) {
  var magazine = req.magazine;

  magazine = _.extend(magazine, req.body);

  magazine.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(magazine);
    }
  });
};

/**
 * Delete an Magazine
 */
exports.delete = function(req, res) {
  var magazine = req.magazine;

  magazine.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(magazine);
    }
  });
};

/**
 * List of Magazines
 */
exports.list = function(req, res) {
  Magazine.find().sort('-created').populate('user', 'displayName').exec(function(err, magazines) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(magazines);
    }
  });
};

/**
 * Magazine middleware
 */
exports.magazineByID = function(req, res, next, id) {

  // if (!mongoose.Types.ObjectId.isValid(id)) {
  //   return res.status(400).send({
  //     message: 'Magazine is invalid'
  //   });
  // }

  Magazine.findOne({ id: id }).exec(function (err, magazine) {
    if (err) {
      return next(err);
    } else if (!magazine) {
      return res.status(404).send({
        message: 'No Magazine with that identifier has been found'
      });
    }
    req.magazine = magazine;
    next();
  });
};
