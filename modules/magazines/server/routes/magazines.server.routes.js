'use strict';

/**
 * Module dependencies
 */
var magazinesPolicy = require('../policies/magazines.server.policy'),
  magazines = require('../controllers/magazines.server.controller');

module.exports = function(app) {
  // Magazines Routes
  app.route('/api/magazines').all(magazinesPolicy.isAllowed)
    .get(magazines.list)
    .post(magazines.create);

  app.route('/api/magazines/:magazineId').all(magazinesPolicy.isAllowed)
    .get(magazines.read)
    .put(magazines.update)
    .delete(magazines.delete);

  // Finish by binding the Magazine middleware
  app.param('magazineId', magazines.magazineByID);
};
