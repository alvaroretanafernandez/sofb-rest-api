'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Magazine Schema
 */
var MagazineSchema = new Schema({
  id: {
    'type': Number
  },
  url: {
    'type': String
  },
  frame: {
    'type': String
  },
  img: {
    'type': String,
    'default': ''
  },
  published: {
    type: Date

  },
  updated: { type: Date, default: Date.now }
});

mongoose.model('Magazine', MagazineSchema);
