'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Link = mongoose.model('Link'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  link;

/**
 * Link routes tests
 */
describe('Link CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Link
    user.save(function () {
      link = {
        name: 'Link name'
      };

      done();
    });
  });

  it('should be able to get a list of Links if not signed in', function (done) {
    // Create new Link model instance
    var linkObj = new Link(link);

    // Save the link
    linkObj.save(function () {
      // Request Links
      request(app).get('/api/links')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Link if not signed in', function (done) {
    // Create new Link model instance
    var linkObj = new Link(link);

    // Save the Link
    linkObj.save(function () {
      request(app).get('/api/links/' + linkObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', link.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Link with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/links/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Link is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Link which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Link
    request(app).get('/api/links/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Link with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should not be able to delete an Link if not signed in', function (done) {
    // Set Link user
    link.user = user;

    // Create new Link model instance
    var linkObj = new Link(link);

    // Save the Link
    linkObj.save(function () {
      // Try deleting Link
      request(app).delete('/api/links/' + linkObj._id)
        .expect(403)
        .end(function (linkDeleteErr, linkDeleteRes) {
          // Set message assertion
          (linkDeleteRes.body.message).should.match('User is not authorized');

          // Handle Link error error
          done(linkDeleteErr);
        });

    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Link.remove().exec(done);
    });
  });
});
