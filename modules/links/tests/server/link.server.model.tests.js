'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    Link = mongoose.model('Link');

/**
 * Globals
 */
var user,
  link;

/**
 * Unit tests
 */
describe('Link Model Unit Tests:', function() {
    beforeEach(function(done) {

        link = new Link({
            name: 'Link Name'
        });

        done();
    });
  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
       link.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) {
      link.name = '';

       link.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Link.remove().exec(function() {
        done();
    });
  });
});
