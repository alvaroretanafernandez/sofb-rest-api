'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Link = mongoose.model('Link'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Link
 */
exports.create = function(req, res) {
  var link = new Link(req.body);
  link.user = req.user;

  link.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(link);
    }
  });
};

/**
 * Show the current Link
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var link = req.link ? req.link.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  link.isCurrentUserOwner = req.user && link.user && link.user._id.toString() === req.user._id.toString();

  res.jsonp(link);
};

/**
 * Update a Link
 */
exports.update = function(req, res) {
  var link = req.link;

  link = _.extend(link, req.body);

  link.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(link);
    }
  });
};

/**
 * Delete an Link
 */
exports.delete = function(req, res) {
  var link = req.link;

  link.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(link);
    }
  });
};

/**
 * List of Links
 */
exports.list = function(req, res) {
  Link.find().sort('-created').populate('user', 'displayName').exec(function(err, links) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(links);
    }
  });
};

/**
 * Link middleware
 */
exports.linkByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Link is invalid'
    });
  }

  Link.findById(id).populate('user', 'displayName').exec(function (err, link) {
    if (err) {
      return next(err);
    } else if (!link) {
      return res.status(404).send({
        message: 'No Link with that identifier has been found'
      });
    }
    req.link = link;
    next();
  });
};
