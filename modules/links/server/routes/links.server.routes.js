'use strict';

/**
 * Module dependencies
 */
var linksPolicy = require('../policies/links.server.policy'),
  links = require('../controllers/links.server.controller');

module.exports = function(app) {
  // Links Routes
  app.route('/api/links').all(linksPolicy.isAllowed)
    .get(links.list)
    .post(links.create);

  app.route('/api/links/:linkId').all(linksPolicy.isAllowed)
    .get(links.read)
    .put(links.update)
    .delete(links.delete);

  // Finish by binding the Link middleware
  app.param('linkId', links.linkByID);
};
