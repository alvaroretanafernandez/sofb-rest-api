'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  path = require('path'),
  mongoose = require('mongoose'),
  Video = mongoose.model('Video');
/**
 * Globals
 */
var user,
  video;

/**
 * Unit tests
 */
describe('Video Model Unit Tests:', function() {
    beforeEach(function(done) {
      video = new Video({
          name: 'Video Name'
      });
      done();
    });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
      video.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) {
      video.name = '';

      video.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Video.remove().exec(function() {
        done();
    });
  });
});
