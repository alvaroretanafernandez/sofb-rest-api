'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Video = mongoose.model('Video'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  video;

/**
 * Video routes tests
 */
describe('Video CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

      video = {
        name: 'Video name'
      };

      done();
  });


  it('should be able to get a list of Videos if not signed in', function (done) {
    // Create new Video model instance
    var videoObj = new Video(video);

    // Save the video
    videoObj.save(function () {
      // Request Videos
      request(app).get('/api/videos')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Video if not signed in', function (done) {
    // Create new Video model instance
    var videoObj = new Video(video);

    // Save the Video
    videoObj.save(function () {
      request(app).get('/api/videos/' + videoObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', video.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Video with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/videos/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Video is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Video which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Video
    request(app).get('/api/videos/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Video with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should not be able to delete an Video if not signed in', function (done) {
    // Set Video user
    video.user = user;

    // Create new Video model instance
    var videoObj = new Video(video);

    // Save the Video
    videoObj.save(function () {
      // Try deleting Video
      request(app).delete('/api/videos/' + videoObj._id)
        .expect(403)
        .end(function (videoDeleteErr, videoDeleteRes) {
          // Set message assertion
          (videoDeleteRes.body.message).should.match('User is not authorized');

          // Handle Video error error
          done(videoDeleteErr);
        });

    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Video.remove().exec(done);
    });
  });
});
