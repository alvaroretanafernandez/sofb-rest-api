'use strict';

/**
 * Module dependencies
 */
var _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    mongoose = require('mongoose'),
    multer = require('multer'),
    config = require(path.resolve('./config/config')),
    User = mongoose.model('User'),
    // Foodlog = mongoose.model('Foodlog'),
    // Meal = mongoose.model('Meal'),
    // Recipe = mongoose.model('Recipe'),
    async = require('async'),
    validator = require('validator');


var whitelistedFields = ['firstName', 'lastName', 'email'];

var LB_VALUE = 0.453; // 1kg = 0.453lb
var FT_VALUE = 0.0328084; // 1cm = 0.0328084foot


/**
 * Update user details
 */
exports.update = function (req, res) {
    // Init Variables
    var user = req.user;

    if (user) {
        // Update whitelisted fields only
        user = _.extend(user, _.pick(req.body, whitelistedFields));

        user.updated = Date.now();
        user.displayName = user.firstName + ' ' + user.lastName;

        user.save(function (err) {
            if (err) {
                return res.status(422).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                req.login(user, function (err) {
                    if (err) {
                        res.status(400).send(err);
                    } else {
                        res.json(user);
                    }
                });
            }
        });
    } else {
        res.status(401).send({
            message: 'User is not signed in'
        });
    }
};


function _calculateAge(dob) { // birthday is a date
    var ageDifMs = Date.now() - dob.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function _calculateBMR(user) {

    var activityMap = {
        sedentary: 1.2,
        lightly: 1.375,
        moderately: 1.55,
        very: 1.725,
        extremly: 1.9
    };

    var _height = user.profile.heightUnits === 'ft' ? user.profile.height / FT_VALUE : user.profile.height;
    var _weight = user.profile.weightUnits === 'lb' ? user.profile.weight * LB_VALUE : user.profile.weight;

    console.log('user_height ' + user.profile.height);
    console.log('_height ' + _height);
    console.log('user_weight ' + user.profile.weight);
    console.log('_weight ' + _weight.toFixed(0));

    var _gFactor = user.profile.gender === 'm' ? 66 : 655;
    var _wFactor = user.profile.gender === 'm' ? 13.7 : 9.6;
    var _hFactor = user.profile.gender === 'm' ? 5 : 1.8;
    var _yFactor = user.profile.gender === 'm' ? 6.8 : 4.7;

    var bmrWeight = _wFactor * _weight.toFixed(0);
    var bmrHeight = _hFactor * _height.toFixed(0);
    var bmrAge = _yFactor * _calculateAge(new Date(user.profile.dob));

    console.log('bmrAge ' + bmrAge);

    var bmr = _gFactor + bmrWeight + bmrHeight - bmrAge;

    console.log('bmr ' + bmr);


    var bmrActivity = bmr * activityMap[user.profile.activity];

    if (user.profile.goal === 'maintain') {
        return Number(bmrActivity);
    }
    if (user.profile.goal === 'gain') {
        return Number(bmrActivity + 500);
    }
    if (user.profile.goal === 'lose') {
        return Number(bmrActivity * 0.8);
    }
}

function _calculateMacro(bmr) {

    return {
        protein: (bmr * 25) / 100,
        fat: (bmr * 30) / 100,
        carb: (bmr * 45) / 100
    };
}


/**
 * Setup profile
 * @param req
 * @param res
 */
exports.profile = function (req, res) {
    // Init Variables
    var user = req.user;
    async.waterfall([
        function (callback) {
            // set profile in user & bmr
            user.profile = req.body;
            user.updated = Date.now();
            user.save(function (err) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else { callback(null, user); }
            });
        }

    ], function (err, result) {
        console.log(result);
        if (!err) {
            result.password = undefined;
            result.salt = undefined;
            res.json({ user: result, isComplete: true });
        } else {
            return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
    });


};


/**
 * Update profile
 * @param req
 * @param res
 */
exports.updateProfile = function (req, res) {
    // Init Variables
    var user = req.user;
    async.waterfall([
        function (callback) {
            // set profile in user & bmr
            user.profile = req.body;
            user.updated = Date.now();
            user.save(function (err) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else { callback(null, user); }
            });
        },
        function (_user, callback) {
            _user.save(function (err) {
                if (err) {
                    callback(err, null);
                } else {
                    req.user = _user;
                    callback(null, _user);
                }
            });
        }
    ], function (err, result) {
        console.log(result);
        if (!err) {
            result.password = undefined;
            result.salt = undefined;
            // console.log(result);
            res.json({ user: result, isUpdated: true });
        } else {
            return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
    });


};


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
    var user = req.user;
    var existingImageUrl;

    // Filtering to upload only images
    var multerConfig = config.uploads.profile.image;
    multerConfig.fileFilter = require(path.resolve('./config/lib/multer')).imageFileFilter;
    var upload = multer(multerConfig).single('newProfilePicture');

    if (user) {
        existingImageUrl = user.profileImageURL;
        uploadImage()
            .then(updateUser)
            .then(deleteOldImage)
            .then(login)
            .then(function () {
                res.json(user);
            })
            .catch(function (err) {
                res.status(422).send(err);
            });
    } else {
        res.status(401).send({
            message: 'User is not signed in'
        });
    }

    function uploadImage() {
        return new Promise(function (resolve, reject) {
            upload(req, res, function (uploadError) {
                if (uploadError) {
                    reject(errorHandler.getErrorMessage(uploadError));
                } else {
                    resolve();
                }
            });
        });
    }

    function updateUser() {
        return new Promise(function (resolve, reject) {
            user.profileImageURL = config.uploads.profile.image.dest + req.file.filename;
            user.save(function (err, theuser) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    function deleteOldImage() {
        return new Promise(function (resolve, reject) {
            if (existingImageUrl !== User.schema.path('profileImageURL').defaultValue) {
                fs.unlink(existingImageUrl, function (unlinkError) {
                    if (unlinkError) {
                        console.log(unlinkError);
                        reject({
                            message: 'Error occurred while deleting old profile picture'
                        });
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }

    function login() {
        return new Promise(function (resolve, reject) {
            req.login(user, function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    resolve();
                }
            });
        });
    }
};

/**
 * Send User
 */
exports.me = function (req, res) {
    User.findById(req.user._id)
        .exec(function (err, user) {
        if (err || !user) {
            return res.status(401).json('user not authorized');
        }
        user.password = undefined;
        user.salt = undefined;
        res.json({ user: user });

    });
};
