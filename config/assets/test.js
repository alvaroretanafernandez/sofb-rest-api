'use strict';

module.exports = {
  tests: {
    server: ['./test.js', 'modules/*/tests/server/**/*.js']
  }
};
