'use strict';

module.exports = {
  app: {
    title: 'SOFB REST API',
    description: 'SOFB REST API JWT AUTH - JavaScript with MongoDB, Express and Node.js',
    keywords: 'SOFB REST API - stoked on fixed bikes - rest api',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  db: {
    promise: global.Promise
  },
  port: process.env.PORT || 3000,
  host: process.env.HOST || '0.0.0.0',
  // DOMAIN config should be set to the fully qualified application accessible
  // URL. For example: https://www.myapp.com (including port if required).
  domain: process.env.DOMAIN || 'https://stokedonfixedbikes.com',
  // Session Cookie settings
  sessionCookie: {
    // session expiration is set by default to 24 hours
    maxAge: 24 * (60 * 60 * 1000),
    // httpOnly flag makes sure the cookie is only accessed
    // through the HTTP protocol and not JS/browser
    httpOnly: true,
    // secure cookie should be turned to true to provide additional
    // layer of security so that the cookie is set only when working
    // in HTTPS mode.
    secure: false
  },
  jwtSecret: process.env.JWT_SECRET || '56bf407fec3joel1nutrifixddde2ba09f90',
  // sessionSecret should be changed for security measures and concerns
  sessionSecret: process.env.SESSION_SECRET || '56bf407fec3joel1nutrifixddde2ba09f90',
  // sessionKey is the cookie session name
  sessionKey: 'sessionId',
  sessionCollection: 'sessions',
  // Lusca config
  csrf: {
    csrf: false,
    csp: false,
    xframe: 'SAMEORIGIN',
    p3p: 'ABCDEF',
    xssProtection: true
  },
  logo: 'modules/core/client/img/brand/logo.png',
  favicon: 'modules/core/client/img/brand/favicon.ico',
  illegalUsernames: ['meanjs', 'administrator', 'password', 'admin', 'user',
    'unknown', 'anonymous', 'null', 'undefined', 'api', 'sofb'
  ],
  uploads: {
    profile: {
      image: {
        dest: './modules/users/server/uploads/',
        limits: {
          fileSize: 1 * 1024 * 1024 // Max file size in bytes (1 MB)
        }
      }
    }
  },
  shared: {
    owasp: {
      allowPassphrases: true,
      maxLength: 20,
      minLength: 6,
      minPhraseLength: 20,
      minOptionalTestsToPass: 4
    }
  }

};
